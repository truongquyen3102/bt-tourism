<div class="listItem">
    <div class="wrapPhoto">
        <p class="photo"><?php thePostThumnail('post-thumbnail'); ?></p>
        <p class="cate notoSerif">
            <?php theTaxsFirst('cate_tourism'); ?>
            <span class="catColor" style="border-bottom-color: #B9C15B"></span>
        </p>
    </div>
    <div class="detailInfor">
        <ul class="cateInfor notoSerif clearfix">
            <?php theTaxsPostSlide('cate_tourism'); ?>
        </ul>
        <h4 class="titleSub notoSerif"><?php the_title(); ?></h4>
        <p class="txtInfor"><?php the_excerpt(); ?></p>
    </div>
</div>