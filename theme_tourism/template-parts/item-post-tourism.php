<div class="listItem">
    <div class="wrapPhoto">
        <p class="photo"><a href="<?php the_permalink(); ?>" class="hover">
		<?php thePostThumnail('post-thumbnail'); ?>
        </a></p> 
        <p class="cate notoSerif">
            <?php theTaxsFirst('cate_tourism'); ?>
            <span class="catColor" style="border-bottom-color: #B9C15B"></span>
        </p>
        <ul class="postTags">
            <?php theListTags('tag_tourism'); ?>
        </ul>
    </div>
    <h3 class="listItemTitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
</div>