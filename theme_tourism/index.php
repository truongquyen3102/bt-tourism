<?php
/**
 * The main template file
 */
get_header(); ?>
<div class="main">
    <p class="mainPhoto">
        <picture>
            <source media="(max-width:768px)" srcset="<?php echo themeUrl(); ?>/assets/images/index/main-sp.jpg"><img src="<?php echo themeUrl(); ?>/assets/images/index/main.jpg" alt=""></picture>
    </p>
    <div class="inner">
        <p class="btnEx"><a href="javascript:void(0);">
                <picture>
                    <source media="(max-width:768px)" srcset="<?php echo themeUrl(); ?>/assets/images/index/btn-ex-sp.png"><img src="<?php echo themeUrl(); ?>/assets/images/index/btn-ex.png"></picture>
            </a></p>
        <div class="choiceLanguage">
            <select>
                <option selected="selected" disabled="disabled">Language</option>
                <option value="">日本語</option>
                <option value="">English</option>
            </select>
        </div>
    </div>
</div>
<div id="content">
    <div class="areaSpecial">
        <div class="inner">
            <div class="wrapTitle">
                <div class="boxTitle">
                    <h2 class="areaTitle notoSerif">三田の旬な情報</h2>
                    <h3 class="enTitle gabriola">Special Contents</h3>
                </div>
            </div>
            <div class="specialSlide">
                <div class="listItem">
                    <div class="wrapPhoto">
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/slide-photo1.png"></a></p>
                        <p class="cate notoSerif"><a href="javascript:void(0);">特集</a><span class="catColor" style="background-color: "></span></p>
                    </div>
                    <h3 class="listItemTitle"><a href="javascript:void(0);" class="hover">ここにテキストが入りますここにテキストが入ります</a></h3>
                </div>
                <div class="listItem">
                    <div class="wrapPhoto">
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/slide-photo2.png"></a></p>
                        <p class="cate notoSerif"><a href="javascript:void(0);">特集</a>
                            <span class="catColor" style="border-bottom-color: #e7a67a; "></span></p>
                    </div>
                    <h3 class="listItemTitle"><a href="javascript:void(0);" class="hover">ここにテキストが入りますここにテキストが入ります</a></h3>
                </div>
                <div class="listItem">
                    <div class="wrapPhoto">
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/slide-photo3.png"></a></p>
                        <p class="cate notoSerif"><a href="javascript:void(0);">特集</a><span class="catColor" style="background-color: "></span></p>
                    </div>
                    <h3 class="listItemTitle"><a href="javascript:void(0);" class="hover">ここにテキストが入りますここにテキストが入ります</a></h3>
                </div>
                <div class="listItem">
                    <div class="wrapPhoto">
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/slide-photo2.png"></a></p>
                        <p class="cate notoSerif"><a href="javascript:void(0);">特集</a><span class="catColor" style="background-color: "></span></p>
                    </div>
                    <h3 class="listItemTitle"><a href="javascript:void(0);" class="hover">ここにテキストが入りますここにテキストが入ります</a></h3>
                </div>
            </div>
            <p class="linkBtn"><a href="javascript:void(0);" class="notoSerif">もっと見る</a></p>
        </div>
    </div>
    <!-- areaSpecial -->
    <div class="areaTourism">
        <div class="inner">
            <div class="wrapTitle">
                <div class="boxTitle">
                    <h2 class="areaTitle notoSerif">観光カテゴリー</h2>
                    <h3 class="enTitle gabriola">Tourism category</h3>
                </div>
            </div>
            <ul class="listTourism">
                <li>
                    <h3 class="listTourismTitle"><img src="<?php echo themeUrl(); ?>/assets/images/index/list-tourism-title-1.png"></h3>
                    <p class="tourismCategory"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/tourism-category1.png"></a></p>
                </li>
                <li>
                    <h3 class="listTourismTitle yellow"><img src="<?php echo themeUrl(); ?>/assets/images/index/list-tourism-title-2.png"></h3>
                    <p class="tourismCategory"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/tourism-category2.png"></a></p>
                </li>
                <li>
                    <h3 class="listTourismTitle orange"><img src="<?php echo themeUrl(); ?>/assets/images/index/list-tourism-title-3.png"></h3>
                    <p class="tourismCategory"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/tourism-category3.png"></a></p>
                </li>
                <li>
                    <h3 class="listTourismTitle pink"><img src="<?php echo themeUrl(); ?>/assets/images/index/list-tourism-title-4.png"></h3>
                    <p class="tourismCategory"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/tourism-category4.png"></a></p>
                </li>
                <li>
                    <h3 class="listTourismTitle violet"><img src="<?php echo themeUrl(); ?>/assets/images/index/list-tourism-title-5.png"></h3>
                    <p class="tourismCategory"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/tourism-category5.png"></a></p>
                </li>
                <li>
                    <h3 class="listTourismTitle green"><img src="<?php echo themeUrl(); ?>/assets/images/index/list-tourism-title-6.png"></h3>
                    <p class="tourismCategory"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/tourism-category6.png"></a></p>
                </li>
            </ul>
        </div>
    </div>
    <!-- areaTourism -->
    <div class="areaModel">
        <div class="wrapTitle">
            <div class="boxTitle">
                <h2 class="areaTitle notoSerif">モデルコース</h2>
                <h3 class="enTitle gabriola">Model course</h3>
            </div>
        </div>
        <div class="inner">
            <ul class="listModel">
                <li>
                    <p class="modelPhoto"><a href="javascript:void(0);"><img src="<?php echo themeUrl(); ?>/assets/images/index/model-photo1.png" alt="散策コース"></a></p>
                    <h3 class="listModelTitle notoSerif"><a href="javascript:void(0);">散策コース</a></h3>
                </li>
                <li>
                    <p class="modelPhoto"><a href="javascript:void(0);"><img src="<?php echo themeUrl(); ?>/assets/images/index/model-photo2.png" alt="グルメコース"></a></p>
                    <h3 class="listModelTitle notoSerif"><a href="javascript:void(0);">グルメコース</a></h3>
                </li>
                <li>
                    <p class="modelPhoto"><a href="javascript:void(0);"><img src="<?php echo themeUrl(); ?>/assets/images/index/model-photo3.png" alt="体験コース"></a></p>
                    <h3 class="listModelTitle notoSerif"><a href="javascript:void(0);">体験コース</a></h3>
                </li>
            </ul>
        </div>
        <p class="linkBtn"><a href="javascript:void(0);" class="notoSerif">もっと見る</a></p>

        <ul class="listAllCate pc">
            <li>
                <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ<br>事務連絡</a></h3>
            </li>
            <li>
                <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo2.png"></a></p>
                <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">さんだ秋の観光と味覚まつり</a></h3>
            </li>
            <li>
                <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo3.png"></a></p>
                <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">さんだ桜まつり2021</a></h3>
            </li>
            <li>
                <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo4.png"></a></p>
                <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">Sanda Air Tourism</a></h3>
            </li>
            <li>
                <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo2.png"></a></p>
                <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">さんだ秋の観光と味覚まつり</a></h3>
            </li>
            <li>
                <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo3.png"></a></p>
                <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">さんだ桜まつり2021</a></h3>
            </li>
        </ul>

        <div class="listAllCatesp sp">
            <div class="listAllItem">
                <ul class="wrapItem">
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                </ul>
            </div>
            <div class="listAllItem">
                <ul class="wrapItem">
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                </ul>
            </div>
            <div class="listAllItem">
                <ul class="wrapItem">
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/all-cate-photo1.png"></a></p>
                        <h3 class="allcateTitle notoSerif"><a href="javascript:void(0);" class="hover">三田市観光協会員の皆様へ事務連絡</a></h3>
                    </li>
                </ul>
            </div>
        </div>

        <div class="fullPhoto">
            <p class="natural">
                <picture>
                    <source media="(max-width:768px)" srcset="<?php echo themeUrl(); ?>/assets/images/index/natural-sp.png"><img src="<?php echo themeUrl(); ?>/assets/images/index/natural.png"></picture>
            </p>
            <p class="title">
                <picture>
                    <source media="(max-width:768px)" srcset="<?php echo themeUrl(); ?>/assets/images/index/natural-title.svg"><img src="<?php echo themeUrl(); ?>/assets/images/index/natural-title.png" alt="ちょこっと自然へin a little natural"></picture>
            </p>
        </div>
    </div>
    <!-- areaModel -->
    <div class="wrapAboutnews">
        <div class="areaAbout">
            <div class="inner">
                <div class="aboutContent">
                    <div class="wrapTitle">
                        <div class="boxTitle">
                            <h2 class="areaTitle notoSerif">癒しのまち・三田市</h2>
                            <h3 class="enTitle gabriola">About Sanda city</h3>
                        </div>
                    </div>
                    <p class="aboutText">ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。<br>ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。<br>ここに280字程度のテキストが入ります。<br>ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。ここに280字程度のテキストが入ります。</p>
                </div>
                <p class="map">
                    <picture>
                        <source media="(max-width:768px)" srcset="<?php echo themeUrl(); ?>/assets/images/index/about-map-sp.png"><img src="<?php echo themeUrl(); ?>/assets/images/index/about-map.png" alt="三田市"></picture>
                </p>
            </div>
            <p class="linkBtn"><a href="javascript:void(0);" class="notoSerif">もっと見る</a></p>
        </div>
        <!-- areaAbout -->
        <div class="areaNews">
            <div class="inner">
                <div class="wrapTitle">
                    <div class="boxTitle">
                        <h2 class="areaTitle notoSerif">お知らせ</h2>
                        <h3 class="enTitle gabriola">News</h3>
                    </div>
                </div>
                <ul class="listNews">
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/news-photo1.png"></a></p>
                        <div class="newsCt">
                            <div class="date notoSerif">2021.10.01</div>
                            <h3 class="newsTitle"><a href="javascript:void(0);" class="hover notoSerif">食欲の秋!三田牛を使ったご飯のお供は<br class="pc">いかが？</a></h3>
                        </div>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/news-photo1.png"></a></p>
                        <div class="newsCt">
                            <div class="date notoSerif">2021.10.01</div>
                            <h3 class="newsTitle"><a href="javascript:void(0);" class="hover notoSerif">食欲の秋!三田牛を使ったご飯のお供は<br class="pc">いかが？</a></h3>
                        </div>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/news-photo2.png"></a></p>
                        <div class="newsCt">
                            <div class="date notoSerif">2021.10.01</div>
                            <h3 class="newsTitle"><a href="javascript:void(0);" class="hover notoSerif">イベントの中止、星空観察会について</a></h3>
                        </div>
                    </li>
                    <li>
                        <p class="photo"><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/news-photo2.png"></a></p>
                        <div class="newsCt">
                            <div class="date notoSerif">2021.10.01</div>
                            <h3 class="newsTitle"><a href="javascript:void(0);" class="hover notoSerif">イベントの中止、星空観察会について</a></h3>
                        </div>
                    </li>
                </ul>
                <p class="linkBtn"><a href="javascript:void(0);" class="notoSerif">一覧へ</a></p>
            </div>
        </div>
        <!-- areaNews -->
    </div>
    <div class="areaInstagram">
        <div class="wrapTitle">
            <div class="boxTitle">
                <h2 class="areaTitle notoSerif">公式インスタグラム</h2>
                <h3 class="enTitle gabriola">Instagram</h3>
            </div>
        </div>
        <ul class="listPhoto">
            <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/insat-photo1.png"></a></li>
            <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/insat-photo2.png"></a></li>
            <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/insat-photo3.png"></a></li>
            <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/insat-photo1.png"></a></li>
            <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/insat-photo2.png"></a></li>
            <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/insat-photo3.png"></a></li>
        </ul>
    </div>
    <!-- areaInstagram -->
    <div class="areaLinkLogo">
        <div class="inner">
            <ul class="listLogo">
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo1.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo2.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo3.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo4.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo5.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo6.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo7.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo8.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo9.png"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/index/link-photo10.png"></a></li>
            </ul>
        </div>
    </div>
    <!-- areaLinkLogo -->
</div>
<!-- #content -->

<?php get_footer(); ?>