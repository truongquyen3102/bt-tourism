/***************************************************************************
 *
 * SCRIPT JS
 *
 ***************************************************************************/

 $(document).ready(function() {

    // event
    if ($('.boxImageNav.eventjs').length) {
        $('.navBig').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.itemImageContent',
        });
        $('.itemImageContent').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.navBig',
            dots: false,
            centerMode: true,
            centerPadding: '0',
            focusOnSelect: true
        });
    };

    if ($(window).width() < 769 &&$('.eventjs').length) {
        $('.postList').slick({
            infinite: true,
            arrows: true,
            dots: true,
        });
    }


 	// tourism
 	if ($('.boxImageNav').length) {
        $('.navBig').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.itemImageContent',
        });
        $('.itemImageContent').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.navBig',
            dots: false,
            centerMode: true,
            centerPadding: '0',
            focusOnSelect: true
        });
    };


    if ($('.centerSlick').length) {
        $('.centerSlick').slick({
            infinite: true,
            arrows: true,
            dots: false,
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        centerMode: true,
                        centerPadding: '55px',
                        slidesToShow: 1,
                        arrows: false,
                        dots: true,
                    }
                }
            ]
        });
    }


    


 });