<?php
/* 
*	Template Name: Tourism
*/ 
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
$query = '';
if( $keyword ){
    $query = new WP_Query(array(
        'post_type' => 'tourism',
        'posts_per_page' => 6,
        's'     => $keyword,
    ));
}
get_header(); ?>
<div id="content">
    <div class="wrapTitle">
        <h2 class="areaTitle notoSerif">観光カテゴリー</h2>
        <h3 class="enTitle gabriola">Tourism category</h3>
    </div>
    <div class="pageTourism">
        <div class="inner">
            <ul class="subCatList notoSerif">
                <?php theTaxs('cate_tourism'); ?>
            </ul>
            <!-- subCatList -->
            <div class="postMobiSearch sp">
                <div class="wrapSearch">
                    <form action="<?php HOME_URL.'/tourism'; ?>" method="get">
                        <input type="text" name="keyword" class="searchInput" placeholder="キーワードで検索する" value="<?php echo $keyword; ?>">
                        <input type="submit" class="searchSubmit" value="Search">
                    </form>
                </div>
            </div>
            <!-- postMobiSearch -->
			<div class="postList">
			    <?php  
                    if( $keyword ){
                         while($query->have_posts()) : $query->the_post();
                            get_template_part( 'template-parts/item-post-tourism');
                        endwhile;
                        wp_reset_postdata();
                    }else{
                        while(have_posts()) : the_post();
                            get_template_part( 'template-parts/item-post-tourism');
                        endwhile;
                    }
			    ?>
			</div>
			<!-- end postList -->

            <?php theme_pagination($query); ?>
            <!-- end pagination -->
        </div>
    </div>
    <!-- pageTourism -->

</div>
<!-- #content -->

<?php get_footer(); ?>
