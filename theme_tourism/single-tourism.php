<?php 
    $size = "full";
    $images = get_field('gallery-single');
    $url_video = get_field('link_video');
    $address = get_field('address');
    $access = get_field('access');
    $tel = get_field('tel');
    $business = get_field('business');
    $holiday = get_field('holiday');
    $address = get_field('address');
    $url_link = get_field('url');
    $googleMap = get_field('google_map');
?>
<?php get_header(); ?>
<div id="content">
    <?php while ( have_posts() ) : the_post(); ?>
    <div class="areaSpecial">
        <div class="inner">
            <div class="titleContent notoSerif">
                <p class="cateContent"><?php theTaxsPost('cate_tourism'); ?></p>
                <h2 class="titleBig"><?php the_title(); ?></h2>
            </div>
            <div class="boxImageNav">
                <div class="navBig">
                    <?php if( has_post_thumbnail() ): ?>
                        <p class="imageContent"><?php thePostThumnail(); ?></p>
                    <?php endif; ?>
                    <?php 
                        if( $images ): 
                            foreach( $images as $image_id ): ?>
                                <p class="imageContent"><?php echo wp_get_attachment_image( $image_id, $size ); ?></p>
                            <?php endforeach;
                        endif;
                    ?>
                </div>
                <div class="itemImageContent">
                    <?php if( has_post_thumbnail() ): ?>
                    <p class="itemImg"><?php thePostThumnail(); ?></p>
                    <?php endif; ?>
                    <?php 
                        if( $images ): 
                            foreach( $images as $image_id ): ?>
                                <p class="itemImg"><?php echo wp_get_attachment_image( $image_id, $size ); ?></p>
                            <?php endforeach;
                        endif;
                    ?>
                </div>
            </div>
            <div class="thecontent">
                <?php the_content(); ?>
            </div>
            <div class="videoContent">
                <?php theIfarmeVideo( $url_video ); ?>
            </div> 
            <div class="boxAddress clearfix">
                <div class="tableContent">
                    <table>
                        <?php if( $address ): ?>
                        <tr><th>住所</th><td><?php echo $address; ?></td></tr>
                        <?php endif; ?>

                        <?php if( $access ): ?>
                        <tr><th>アクセス</th><td><?php echo $access; ?></td></tr>
                        <?php endif; ?>

                        <?php if( $tel ): ?>
                        <tr><th>TEL</th><td><a href="tel:<?php echo str_replace( '-', '', $tel ); ?>" class="phoneTable"><?php echo $tel; ?></a></td>
                        </tr>
                        <?php endif; ?>

                        <?php if( $business ): ?>
                        <tr><th>営業時間</th><td><?php echo $business;?></td></tr>
                        <?php endif; ?>

                        <?php if( $holiday ): ?>
                        <tr><th>定休日</th><td><?php echo $holiday;?></td></tr>
                        <?php endif; ?>

                        <?php if( $url_link ): ?>
                        <tr><th>URL</th><td><a href="<?php echo $url_link;?>" target="_blank"><?php echo $url_link;?></a></td></tr>
                        <?php endif; ?>
                    </table>
                </div>
                <div class="mapContent">
                    <?php if( $googleMap ) echo $googleMap; ?>
                </div>
            </div>
            <p class="linkBtn"><a href="javascript:void(0);" class="notoSerif">もっと見る</a></p>
        </div>
    </div>
    <?php endwhile; ?>
    <!-- areaSpecial -->

    <div class="areaInformation">
        <div class="inner">
            <h3 class="titleInfor notoSerif">おすすめスポット</h3>
            <div class="postList centerSlick notoSerif">
            <?php 
                $termRelated = [];
                $currentterm = get_the_terms($post->ID, 'cate_tourism' ); 
                foreach ($currentterm as $term) {
                    array_push($termRelated, $term->slug);
                }
                $the_query = new WP_Query([
                    'post_type' => 'tourism',
                    'posts_per_page' => -1,
                    'post__not_in' => array($post->ID),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'cate_tourism',
                            'field'    => 'slug',
                            'terms'    => $termRelated,
                        ),
                    )
                ]);
                while ( $the_query->have_posts() ) : $the_query->the_post();
                    get_template_part( 'template-parts/item-tourism-slide' );
                endwhile;
                wp_reset_postdata();
            ?>
            </div>
        </div>
    </div>
    <!-- areaInformation -->

</div>
<!-- #content -->

<?php get_footer(); ?>