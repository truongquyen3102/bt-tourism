<?php
/**
 * ADD MAIN FUNCTIONS
 */


// HOOK //


// EXCERPT LENGTH CONTENT
add_filter('excerpt_length','new_excerpt_length');
function new_excerpt_length( $more ){
    return 55;
}

// THE LIST TAX AJAX
function theTaxsAjax( $tax_slug = 'category' ) {
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    // GET RESULT
    $taxs_html = '';
    $taxonomies = get_terms( $tax_slug );
    foreach($taxonomies as $taxonomy) {
        if($taxonomy->term_id != 1) {
            $taxs_html .= '<li><a href="javascript:void(0);" data-term="'.$taxonomy->slug.'">'.$taxonomy->name.'</a></li>';
        }
    }
    echo $taxs_html;
}

// AJAX JS THE POST
add_action( 'wp_footer', 'ajax_the_post_javascript' );
function ajax_the_post_javascript() { ?>
    <script type="text/javascript" >
        (function($){
        $(document).ready(function(){
            // AJAX SHOW POST
            $(document).on('click', '.pagiNav .pagiItems li a,.subCatList li a,.wrapSearch input[name=search]', function(){
                if( $(this).closest('.subCatList').length > 0 ){
                    $(this).parent().addClass('active').siblings().removeClass('active');
                    var term = $(this).data('term');
                }else{
                    term = $('.subCatList li.active a').data('term');
                    var paged = $(this).data('paged');
                    var keyword = $(this).siblings('input[name=keyword]').val();
                }
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    dataType: 'json',
                    data: {
                        action: 'the_show_post',
                        term:term,
                        paged:paged,
                        keyword:keyword
                    },
                    beforeSend: function() {
                        $('.postList').addClass('loading'); 
                    },
                    success: function (response) {
                        $('.postList').html(response.data.html_post);
                        $('.pagiNav').html(response.data.html_pagi);
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        console.log( 'The following error occured: ' + textStatus, errorThrown );
                    }
                }).complete(function() {
                    $('.postList').removeClass('loading');  
                });
            });
        });
        })(jQuery)
    </script> <?php
}

//AJAX THE SHOW POST 
add_action('wp_ajax_the_show_post', 'the_show_post');
add_action('wp_ajax_nopriv_the_show_post', 'the_show_post');
function the_show_post() {
    $term_slug = isset($_POST['term']) ? $_POST['term'] : '';
    $paged = isset($_POST['paged']) ? $_POST['paged'] : '';
    $keyword = isset($_POST['keyword']) ? $_POST['keyword'] : '';

    $args = array(
        'post_type' => 'tourism',
        'posts_per_page' => 6,
        'paged' => $paged,
    );
    if( $term_slug ){
        $args['tax_query'][] = array(
            'taxonomy' => 'cate_tourism',
            'field'    => 'slug',
            'terms'    => $term_slug,
        );
    }
    if( $keyword ){
        $args['s'] = $keyword;
    }

    $query = new WP_Query( $args );
    if( $query ) {
        //GET DATA PAGINATION
        $html_pagi .= theme_pagination_ajax($query ,$paged);
        //GET DATA POSTS
        ob_start();
            while($query->have_posts()) : $query->the_post();
                get_template_part( 'template-parts/item-post-tourism');
            endwhile;
            wp_reset_postdata(); 
            $html_post = ob_get_contents();
        ob_end_clean();
        //RETURN DATA JSON
        if( !$html_post ) $html_post = "No posts found!!!";
        $result =[
            'html_post' => $html_post,
            'html_pagi' => $html_pagi,  
        ];
        wp_send_json_success($result);
        die();
    }
    wp_send_json_error();
    die();
}

//AJAX THEME PAGINATION  
function theme_pagination_ajax( $post_query = null , $paged = null ) {
    $html = '';
    if( empty( $paged ) ) $paged = 1;
    $prev = $paged - 1;             
    $next = $paged + 1;
    
    $end_size = 0;
    $mid_size = 2;
    $show_all = false;
    $dots = false;
    if( isset($post_query) && $post_query ) {
        $pagi_query = $post_query;
    }
    if( ! $total = $pagi_query->max_num_pages ) $total = 1;

    if( $total > 1 )
    {
        $html .= '<div class="pagiNav">';
        $html .= '<ul class="pagiItems">';
        if( $paged > 1 ){
            $html .= ' <li class="control prev"><a href="javascript:void(0);" data-paged="'.$prev.'"><img src="'.THEME_URL.'/assets/images/tourism/pagi_prev.png" alt="Prev"></a></li>';
        }
        for( $i = 1; $i <= $total; $i++ ){
            if ( $i == $paged ){
                $html .= '<li class="active"><a>'. $i .'</a></li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $paged && $i >= $paged - $mid_size && $i <= $paged + $mid_size ) || $i > $total - $end_size ) ){
                    $html .= '<li><a href="javascript:void(0);" data-paged="'.$i.'"> '. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    $html .= '<li class="dots"><a>...</a></li>';
                    $dots = false;
                }
            }
        }
        if( $paged < $total ){
            $html .= ' <li class="control next"><a href="javascript:void(0);" data-paged="'.$next.'"><img src="'.THEME_URL.'/assets/images/tourism/pagi_next.png" alt="Prev"></a></li>';
        }
        $html .= '</ul>';
        $html .= '</div>';
    }
    return $html;
}           


// FUNCTION //


// THE TAXS IN A POST
function theTaxsPost( $tax_slug = 'category' ) {
    global $post;
    if( !$post ) return false;
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    // GET RESULT
    $taxs_html = '';
    $taxonomies = wp_get_post_terms( $post->ID, $tax_slug );
    foreach($taxonomies as $taxonomy) {
        if($taxonomy->term_id != 1) {
            $taxs_html .= '<a href="'. get_term_link($taxonomy) .'">'.$taxonomy->name.'</a>';
        }
    }
    echo $taxs_html;
}

// THE TAXS IN A FIRST SLIDE
function theTaxsFirst( $tax_slug = 'category' ) {
    global $post;
    if( !$post ) return false;
    $taxs_html = '';
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    $terms = get_the_terms( $post->ID, $tax_slug );
    if($terms){
        $taxs_html .= '<a href="'. get_term_link($terms[0]) .'">'.$terms[0]->name.'</a>';
    }
    echo $taxs_html;
}

// THE TAXS IN A POST SLIDE
function theTaxsPostSlide( $tax_slug = 'category' ) {
    global $post;
    if( !$post ) return false;
    $taxs_html = '';
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    $terms = get_the_terms( $post->ID, $tax_slug );
    foreach($terms as $term) {
        $taxs_html .= '<li><a href="'. get_term_link($term) .'">'.$term->name.'</a></li>';
    }
    echo $taxs_html;
}

// THE LIST TAX
function theTaxs( $tax_slug = 'category' ) {
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    // GET RESULT
    $class= '';
    $taxs_html = '';
    $termCurrentId = get_queried_object_id();
    $taxonomies = get_terms( $tax_slug );
    foreach($taxonomies as $taxonomy) {
        $class = ( $termCurrentId && $termCurrentId == $taxonomy->term_id ) ? 'active' : '';
        if($taxonomy->term_id != 1) {
            $taxs_html .= '<li class="'.$class.'"><a href="'. get_term_link($taxonomy) .'">'.$taxonomy->name.'</a></li>';
        }
    }
    echo $taxs_html;
}

// THE LIST TAGS
function theListTags( $tax_slug = 'category'){
    global $post;
    if( !$post ) return false;
    $tax_slug = ($tax_slug) ? $tax_slug : 'category';
    // GET RESULT
    $taxs_html = '';
    $taxonomies = wp_get_post_terms( $post->ID, $tax_slug );
    foreach($taxonomies as $taxonomy) {
        if($taxonomy->term_id != 1) {
            $taxs_html .= '<li><a href="'. get_term_link($taxonomy) .'"># '.$taxonomy->name.'</a></li>';
        }
    }
    echo $taxs_html;
}

//THE POST THUMNAIL
function thePostThumnail( $size = '' ){
    if ( has_post_thumbnail() ) {
        the_post_thumbnail($size);
    }else{
        $images = wp_get_attachment_image_src( 70 , $size );
        echo '<img src="'.$images[0].'"';
    }
}

//THE IFRAME VIDEO
function theIfarmeVideo( $url = '' ){
    if( !$url ) return false;
    parse_str( parse_url( $url, PHP_URL_QUERY ), $link );
    if( $link && $link['v'] ){
        echo '<iframe width="100%" height="365px" src="https://www.youtube.com/embed/'.$link['v'].'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>';
    }
    return false;
}

// THEME PAGINATION FUNCTION
function theme_pagination( $post_query = null ) {
    global $paged, $wp_query;
    
    if( empty( $paged ) ) $paged = 1;
    $prev = $paged - 1;             
    $next = $paged + 1;
    
    $end_size = 0;
    $mid_size = 2;
    $show_all = false;
    $dots = false;

    $pagi_query = $wp_query;
    if( isset($post_query) && $post_query ) {
        $pagi_query = $post_query;
    }
    if( ! $total = $pagi_query->max_num_pages ) $total = 1;
    
    if( $total > 1 )
    {
        echo '<div class="pagiNav">';
        echo '<ul class="pagiItems">';
        
        if( $paged > 1 ){
            echo ' <li class="control prev"><a href="'. previous_posts(false) .'"><img src="'.THEME_URL.'/assets/images/tourism/pagi_prev.png" alt="Prev"></a></li>';
        }

        for( $i = 1; $i <= $total; $i++ ){
            if ( $i == $paged ){
                echo '<li class="active"><a>'. $i .'</a></li>';
                $dots = true;
            } else {
                if ( $show_all || ( $i <= $end_size || ( $paged && $i >= $paged - $mid_size && $i <= $paged + $mid_size ) || $i > $total - $end_size ) ){
                    echo '<li><a href="'. get_pagenum_link($i) .'">'. $i .'</a></li>';
                    $dots = true;
                } elseif ( $dots && ! $show_all ) {
                    echo '<li class="dots"><a>...</a></li>';
                    $dots = false;
                }
            }
        }
      
        if( $paged < $total ){
            echo ' <li class="control next"><a href="'. next_posts(0,false) .'"><img src="'.THEME_URL.'/assets/images/tourism/pagi_next.png" alt="Prev"></a></li>';
        }

      	echo '</ul>';
      	echo '</div>';
    }
}


// GET QUERY PAGED NUMBER 
function get_query_paged() {
	return (get_query_var('paged')) ? get_query_var('paged') : 1;
}
