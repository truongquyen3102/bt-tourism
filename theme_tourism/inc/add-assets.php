<?php
/**
 *  SETUP ASSETS
 */

// ADD ASSETS HEAD
add_action('wp_head', 'add_theme_assets_for_head', 50);
function add_theme_assets_for_head() {
?>
	<!-- THEME STYLES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+JP:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php themeUrl(); ?>/assets/libs/slick/slick.css"/>
    <link rel="stylesheet" href="<?php themeUrl(); ?>/assets/libs/slick/slick-theme.css"/>
	<link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/common.css">
    <?php if( is_home() || is_front_page()  ): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/index.css">
    <?php elseif( is_category() || is_archive() || is_page() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/tourism.css">
    <?php elseif( is_single() ): ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/tourism-detail.css">
    <?php
        elseif( is_page() ):
        global $post;
        $post_slug = $post->post_name;
        $page_css_uri = THEME_PATH.'/assets/css/'.$post_slug.'.css';
        if( is_file($page_css_uri) ):
    ?>
        <link rel="stylesheet" type="text/css" href="<?php themeUrl(); ?>/assets/css/<?php echo $post_slug; ?>.css">
    <?php endif; endif; ?>
    
    <!-- THEME SCRIPTS -->
    <script type="text/javascript" src="<?php themeUrl(); ?>/assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?php themeUrl(); ?>/assets/libs/slick/slick.min.js"></script>
    <script type="text/javascript">
        var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    </script>

<?php
}

// ADD ASSETS FOOTER
add_action( 'wp_footer', 'add_theme_assets_for_footer', 50 );
function add_theme_assets_for_footer() {
?>  
    <!-- THEME SCRIPTS -->
    <?php if( is_single() && is_singular('tourism') ): ?>
        <script type="text/javascript" src="<?php themeUrl(); ?>/assets/js/detail.js"></script>
    <?php endif; ?>
	<script type="text/javascript" src="<?php themeUrl(); ?>/assets/js/script.js"></script>
<?php
}