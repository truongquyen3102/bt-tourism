<?php
/**
 * The template for displaying the footer
 */
?>
<div id="footer">
        <div class="choiceLanguage pc">
            <select>
                <option selected="selected" disabled="disabled"></option>
                <option value="">日本語</option>
                <option value="">English</option>
            </select>
        </div>
        <div class="inner">
            <div class="ftLogo"><a href="javascript:void(0);" class="hover">
                <picture><source media="(max-width:768px)" srcset="<?php echo themeUrl(); ?>/assets/images/common/footer-logo-sp.svg"><img src="<?php echo themeUrl(); ?>/assets/images/common/footer-logo.png"></picture>
            </a></div>
            <div class="boxInfo">
                <h3 class="infoTitle notoSerif">三田市観光協会</h3>
                <p class="info notoSerif">〒669-1531<br>三田市天神1丁目10-14 兵庫県阪神北県民局三田庁舎内<br><span class="textTel">TEL: <a href="tel:0795612241" class="tel">079-561-2241</a></span><br><span class="textFax">FAX: <a href="javascript:void(0);" class="fax">079-550-9011</a></span></p>
            </div>
            <ul class="listSoci notoSerif">
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/common/icon-twitter.svg"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/common/icon-facebook.svg"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/common/icon-youtube.svg"></a></li>
                <li><a href="javascript:void(0);" class="hover"><img src="<?php echo themeUrl(); ?>/assets/images/common/icon-insata.svg"></a></li>
            </ul>
        </div>
        <p id="copyright" class="notoSerif">© Copyright 2021 - 三田市観光サイト</p>
    </div>
    <!-- #footer -->
    <?php if( is_home() ){ ?>
        <script type="text/javascript">
            $('.areaSpecial .specialSlide').slick({
                dots: false,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                    }
                }]
            });

            $('.listAllCate').slick({
                centerMode: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                autoplay: false,
                arrows: false,
                variableWidth: true,
            });

            $('.listAllCatesp').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                autoplay: false,
                arrows: false,
            });

            if ($(window).width() < 768) {
                $('.areaModel .listModel').slick({
                    centerMode: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false,
                    autoplay: false,
                    arrows: false,
                    variableWidth: true,
                });
            };

            function menuscroll() {
                var he = $('.main .btnEx').offset().top;
                $(window).scroll(function() {
                    if($(window).width() > 768){
                        if ($(this).scrollTop() > he + 235) {
                            $('.main .choiceLanguage').addClass('fixedBot');
                            $('.main .btnEx').addClass('changeh');
                        } else {
                            $('.main .choiceLanguage').removeClass('fixedBot');
                            $('.main .btnEx').removeClass('changeh');
                        }
                    }
                    else{
                        if ($(this).scrollTop() > he + 235) {
                            $('.main .choiceLanguage').addClass('fixedBot');
                            $('.main .btnEx').addClass('changeh');
                        } else {
                            $('.main .choiceLanguage').removeClass('fixedBot');
                            $('.main .btnEx').removeClass('changeh');
                        }
                    }
                });
            }

            $(window).resize(function(event) {
                menuscroll();
            });
            $(window).load(function(event) {
                menuscroll();
            });
        </script>
    <?php } ?>
    <script type="text/javascript">
        if($(window).width() > 768){
            $('.boxSearch .searchIcon').hover(function(event) {
                $('.boxSearch .headerSearch').addClass('show');
            });
        }
    </script>
	<?php wp_footer(); ?>
</body>
</html>