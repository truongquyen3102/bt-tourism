
<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
</head>
<body>
    <div id="header">
        <div class="inner">
            <div class="headerBar">
                <div class="logo"><a class="hover" href="<?php homeUrl(); ?>">
                    <picture><source media="(max-width:768px)" srcset="<?php echo themeUrl(); ?>/assets/images/common/header-logo-sp.svg"><img src="<?php echo themeUrl(); ?>/assets/images/common/header-logo.png" alt="logo"></picture> 
                </a>
                </div>
                <div class="hamburger sp">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="mainMenu">
                <ul class="menu notoSerif">
                    <li><a href="#" class="hover"><span class="text">観光スポット</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>

                    <li><a href="#" class="hover"><span class="text">グルメ</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>

                    <li><a href="#" class="hover"><span class="text">レジャー</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>

                    <li><a href="#" class="hover"><span class="text">お土産</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>

                    <li><a href="#" class="hover"><span class="text">宿泊</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>

                    <li><a href="#" class="hover"><span class="text">モデルコース</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>

                    <li><a href="#" class="hover"><span class="text">アクセス</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>

                    <li><a href="#" class="hover"><span class="text">お問い合わせ</span><span class="sp arrow"><img src="<?php echo themeUrl(); ?>/assets/images/common/arrow-menu.png"></span></a></li>
                </ul>
                <div class="boxSearch">
                    <form action="">
                        <input class="headerSearch" type="search" name="" placeholder="キーワードで検索する">
                        <button type="submit" class="searchIcon pc"><img src="<?php echo themeUrl(); ?>/assets/images/common/icon-search.svg"></button>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- #header -->
    <div id="fixH"></div>