<?php
/**
 * The template for displaying Category pages
 */
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
$currentTaxs = get_queried_object();
$query = '';
if( $keyword && $currentTaxs ){
    $query = new WP_Query(array(
        'post_type' => 'tourism',
        'posts_per_page' => 6,
        'paged' => get_query_paged(),
        's'     => $keyword,
        'tax_query' => array(
            array(
                'taxonomy' => 'tag_tourism',
                'field'    => 'slug',
                'terms'    => $currentTaxs->slug,
            ),
        ),
    ));
}
get_header(); ?>
 <div id="content">
        <div class="wrapTitle">
            <h2 class="areaTitle notoSerif"><?php echo single_term_title(); ?></h2>
            <h3 class="enTitle gabriola">Tourism tag</h3>
        </div>
        <div class="pageTourism">
            <div class="inner">
                <ul class="subCatList notoSerif">
                    <?php theTaxs('tag_tourism'); ?>
                </ul>
                <!-- subCatList -->

                <div class="postMobiSearch sp">
                    <div class="wrapSearch">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="get">
                            <input type="text" name="keyword" class="searchInput" placeholder="キーワードで検索する" value="<?php echo $keyword; ?>">
                            <input type="submit" class="searchSubmit" value="Search">
                        </form>
                    </div>
                </div>
                <!-- postMobiSearch -->

                <div class="postList">
                    <?php  
                        if( $keyword && $currentTaxs ){
                            while($query->have_posts()) : $query->the_post();
                                get_template_part( 'template-parts/item-post-tourism');
                            endwhile;
                            wp_reset_postdata();
                        }else{
                            while(have_posts()) : the_post();
                                get_template_part( 'template-parts/item-post-tourism');
                            endwhile;
                        }
                    ?>
                </div>
                <!-- endpostList -->

                <?php theme_pagination($query); ?>
                <!-- end pagination -->
            </div>
        </div>
        <!-- pageTourism -->

    </div>
    <!-- #content -->
<?php get_footer(); ?>